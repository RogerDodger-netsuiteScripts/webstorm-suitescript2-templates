    /**
     * <code>afterUpdate</code> event handler
     * 
     * @governance XXX
     * 
     * @param params
     * 		{Object}
     * @param params.fromVersion
     * 		{Number} The version of the bundle that was previously installed
     * @param params.toVersion
     * 		{Number} The version of the bundle that was just installed
     * 
     * @return {void}
     * 
     * @static
     * @function afterUpdate
     */
    function afterUpdate(params) {
        // TODO
    }

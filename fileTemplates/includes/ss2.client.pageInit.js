    /**
     * <code>pageInit</code> event handler
     * 
     * @governance XXX
     * 
     * @param context
	 * 		{Object}            
     * @param context.mode
	 * 		{String} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     * 
     * @return {void}
     * 
     * @static
     * @function pageInit
     */
    function pageInit(context) {
    	// TODO
    }

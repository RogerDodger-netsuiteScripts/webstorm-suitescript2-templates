    /**
     * <code>onAction</code> event handler
     * 
     * @governance XXX
     *
     * @param context
	 * 		{Object}
     * @param context.newRecord
	 *		{Record} The new record with all changes. <code>save()</code> is not
	 *		permitted.
     * @param context.oldRecord
	 * 		{Record} The old record with all changes. <code>save()</code> is not
	 * 		permitted.
	 * @param context.form
     *        {serverWidget.Form} The record form loaded in the UI
     * @param context.type
     *        {String} The event type, such as create, edit, view, delete, etc
     * @param context.workflowId
     *        {String} The internal ID of the workflow that invoked this script
     * 
     * @return {void}
     * 
     * @static
     * @function onAction
     */
    function onAction(context) {
        // TODO
    }

    /**
     * <code>afterInstall</code> event handler
     * 
     * @governance XXX
     * 
     * @param params
     * 		{Object}
     * @param params.version
     * 		{Number} The version of the bundle that was just installed
     * 
     * @return {void}
     * 
     * @static
     * @function afterInstall
     */
    function afterInstall(params) {
        // TODO
    }
